# Write a function named displayInventory() that would take any possible 
# “inventory” and display it
stuff = {'rope': 1, 'torch': 6, 'platinum pieces': 42, 'dagger': 1, 
'deathfist belts': 12}

def displayInventory(inventory):
    print("Inventory:\n")
    item_total = 0
    for k, v in inventory.items():
        item_total += v 
        print(v, k, '\n')
    print("Total number of items: " + str(item_total) + '\n' )

displayInventory(stuff)

# Write a function named addToInventory(inventory, addedItems), where the 
# inventory parameter is a dictionary representing the player’s inventory 
# (like in the previous project) and the addedItems parameter is a list
# like dragonLoot. The addToInventory() function should return a dictionary
# that represents the updated inventory. Note that the addedItems list can
# contain multiples of the same item.

def addToInventory(inventory, addedItems):
    for i in range(len(addedItems)):
        if (addedItems[i] not in inventory.keys()):
            inventory[addedItems[i]] = 1
        else:
            inventory[addedItems[i]]+=1
    return inventory

inv = {'gold coin': 42, 'rope': 1}
dragonLoot = ['gold coin', 'dagger', 'gold coin', 'gold coin', 'ruby']
inv = addToInventory(inv, dragonLoot)
displayInventory(inv)