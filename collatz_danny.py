print('\n## The Collatz Sequence a.k.a. the 3n + 1 problem. ## \n')
print('Defined as follows: ')
print('Start with any positive integer n.')
print('For the next term, if the previous term is even, divide by 2.')
print('If the previous term is odd, multiply by 3 and add 1. \n ')
print('The conjecture states that the sequence will always reach 1. \n')

# Collatz function
def collatz(number):
    if number % 2 == 0:
        return number // 2
    else:
        return 3 * number + 1

def printError():
    print('\n## Invalid Entry ##\n') 

while (True):
    print('Enter a positive integer or 0 to quit: ')  
    iter = 0

    # try/except block to handle incorrect input
    try:
        num = int(input())
    except:
        printError()
        continue
    
    if (num == 0):
        break
    elif (num < 0):
        printError()
        continue

    while (num != 1):
        num = collatz(num)
        print(num)
        iter += 1

    print('This sequence went through ' + str(iter) + ' iteration(s)')

print('Exiting... (Have a good day)')