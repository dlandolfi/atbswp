# Write a function that takes a list value as an argument and returns a string
# with all the items separated by a comma and space, with *and* inserted before
# the last item.
#
# For example, passing spam = ['apples', 'bananas', 'tofu', 'cats'] would
# return 'apples, bananas, tofu, and cats'. But your function should be 
# able to work with any list value passed to it.

def commacode(spam):
    string = ''
    for i in range (len(spam)):
        string += str(spam[i])
        if (i < len(spam) - 2):
            string += ', '
        elif (i < len(spam) -1):
            string += ', and '
        
    return string

spam = ['apples', 'bananas', 'tofu', 'cats', '42', 2*2]
print(commacode(spam))