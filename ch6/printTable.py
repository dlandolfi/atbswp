tableData = [['apples', 'oranges', 'cherries', 'banana'],
             ['Alice', 'Bob', 'Carol', 'David'],
             ['dogs', 'cats', 'moose', 'goose']]

def printTable(tableData):
    colWidths = [0] * len(tableData) # list to store max width of each column
    for list in tableData:
        for data in list:
            if len(data) > colWidths[tableData.index(list)]:
                colWidths[tableData.index(list)]=len(data)
    
    for i in range(len(tableData[0])):
        for j in range(len(tableData)):
            print(tableData[j][i].rjust(colWidths[j]),end=' ')
        print()

printTable(tableData)